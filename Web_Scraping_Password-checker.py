from bs4 import BeautifulSoup as bs
import requests
import hashlib


class password:
    def __init__(self, psw):
        self.psw = psw

    def parameters_check(self):
        '''
        Checking if psw contains:
            - at least 1 uppercase
            - at least 1 lowercase
            - at least 1 digit
            - at least 8 characters in total

        Return:
            - out: str, with all the parameters NOT respected
            - check: bool, with FALSE if fails, TRUE otherwise
        '''
        out = ''
        check = False
        lenflag = len(self.psw) >= 8
        if not lenflag:
            out = out + (f'Password \'{self.psw}\' is not long enough\n')
        uflag = any(ch.isupper() for ch in self.psw)
        if not uflag:
            out = out + (f'Password \'{self.psw}\' does not contain uppercase characters\n')
        lflag = any(ch.islower() for ch in self.psw)
        if not lflag:
            out = out + (f'Password \'{self.psw}\' does not contain lowercase characters\n')
        dflag = any(ch.isdigit() for ch in self.psw)
        if not dflag:
            out = out + (f'Password \'{self.psw}\' does not contain digits\n')
        if lenflag and uflag and lflag and dflag:
            out = out + (f'Password \'{self.psw}\' contains all requested parameters\n')
            check = True
        return out, check


while True:
    print('\n########## THIS PROGRAM EVALUATES THE SECURITY OF A PASSWORD ##########')
    # asking for password input
    str_psw = input(
        'Write a password at least 8-charachters-long with UPPERCASES, lowercases and digits:\n(type 0 to exit)\n')
    # close app if password == 0
    if str_psw == '0':
        print('Program closed, Good Bye')
        break
    # checking password parameters
    else:
        psw = password(str_psw)
        out, check_o = psw.parameters_check()
        print(out)
        if check_o:
            # enconding password via MD5
            md5psw = hashlib.md5(str_psw.encode())
            md5psw = md5psw.hexdigest()
            print(f'The MD5 version of your password is:\n{md5psw}')
            # check if password is safe
            request = requests.get(f'http://md5online.it/index.lm?key_decript={md5psw}')
            soup = bs(request.content, 'html.parser')
            result = str(soup.findAll('font')[3])
            if 'NESSUN RISULTATO' in result:
                print('\n\U0001F600 \U0001F600 \U0001F600 YOUR PASSWORD IS SAFE \U0001F600 \U0001F600 \U0001F600')
            else:
                print('\n\U0001F6A8 \U0001F6A8 \U0001F6A8 YOUR PASSWORD IS NOT SAFE \U0001F6A8 \U0001F6A8 \U0001F6A8')
    continue
