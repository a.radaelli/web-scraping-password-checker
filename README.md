# Web Scraping - Password checker

## First things first

This project is a homework project I submitted during my programming course.
Thanks to this experience I learnt:
- web scraping logic (HTML) and libraries like BeautifulSoup
- how to connect Python to a specific URL and scrape data
- how password coding works and what MD5 is
- control user input

## Description

By connecting to a website for MD5 decripting, if the website return 'no results' than the password can be considered secure because the the connection between the MD5 encription and the password string is non known.
If the website return de decripted version of the string however, the password cannot be considered secure.
